import torch
from torch import nn


class TSNE(nn.Module):
    def __init__(self, n_points, n_dim):
        self.n_points = n_points
        self.n_dim = n_dim
        super().__init__()
        # Logit of datapoint-to-topic weight
        self.logits = nn.Embedding(n_points, n_dim)

    def forward(self, pij, i, j):
        # TODO: реализуйте вычисление матрицы сходства для точек отображения и расстояние Кульбака-Лейблера
        # pij - значения сходства между точками данных
        # i, j - индексы точек
        x = self.logits.weight

        n_obs, dim = x.size()
        xk = x.unsqueeze(0).expand(n_obs, n_obs, dim)
        xl = x.unsqueeze(1).expand(n_obs, n_obs, dim)
        dkl = ((xk - xl)**2.0).sum(2).squeeze()
        n_diagonal = dkl.size()[0]
        part = (1 + dkl).pow(-1.0).sum() - n_diagonal

        xi = self.logits(i)
        xj = self.logits(j)

        num = ((1. + (xi - xj)**2.0).sum(1)).pow(-1.0).squeeze()

        qij = num / part.expand_as(num)

        loss_kld = pij * (torch.log(pij) - torch.log(qij))
        
        return loss_kld.sum()

    def __call__(self, *args):
        return self.forward(*args)
